/**
 * Takes a function that returns a promise and runs it until either it resolves
 * or max tries are exhausted
 * @alias _.PromiseRunner
 */
class PromiseRunner {
  /**
   * Creates the PromiseRunner class instance
   * @param {number} maxAttempts the number of total retries allowed
   * @param {number} delay the delay in ms between retry attempts
   * @param {function} fn the function that returns a promise
   */
  constructor(maxAttempts, delay, fn) {
    // provided parameters
    this.maxAttempts = maxAttempts;
    this.delay       = delay;
    this.fn          = fn;

    // other parameters
    this.attempt = 0;
    this.errors  = {
      last: false,
      all:  []
    };
  }

  /**
   * runs the retry attempts recursively
   * @param {function} resolve the parent resolve method
   * @param {function} reject the parent reject method
   */
  doTry(resolve, reject) {
    this.attempt++;
    this.fn().then((result) => {
      return resolve(result);
    }).catch((e) => {
      this.errors.last = e;
      this.errors.all.push(e);
      if (this.attempt < this.maxAttempts || this.maxAttempts === -1) {
        setTimeout(() => {
          this.doTry(resolve, reject);
        }, this.delay);
      } else {
        return reject(this.errors);
      }
    });
  }

  /**
   * starts the retry attempt chain
   * @return {promise} promise for the entire chain. resolves on success;
   *                   rejects with full errors stack on timeout failure
   */
  go() {
    return new Promise((resolve, reject) => {
      return this.doTry(resolve, reject);
    });
  }
};

module.exports = PromiseRunner;
