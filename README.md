# fortisbot

fortisbot is a fork of the auriga.io bot framework, located [here](http://htmlpreview.github.io/?https://bitbucket.org/Frosthaven/auriga.io).

# auriga.io

auriga.io is a platform agnostic community bot that provides users with commands that they can run. Initially built for support with twitch chat and discord chat, auriga.io can take you as far as you want it to with its extendable class-inheritance based modular design and tight permission controls.

---

# Getting Started

## Requirements
you will need `node version > 8`, and `git` installed. You can acquire git on Windows from [this link](https://git-scm.com/download/win).

## Initializing The Repository
Once the repo is cloned, run `npm install` to acquire all dependencies

---

## The Modular Approach
Modules come in 3 hierarchical flavors: `service` > `plugin` > `command`.

* Services are top level and typically refer to the platform they represent (Discord, Twitch, etc).
* Plugins are where shared code and api wrappers are stored.
* Commands are the active interactions that users will engage with.

---

## Configuration
The default configuration file is located in the `app` folder and named `config.example.yaml`. Make a copy of the file, naming it `config.yaml` (and editing the parameters as desired).

By default all services, plugins, and commands are disabled. You will want to set `enabled:true` for each module you wish to use and fill out any other required information as listed. Setting permissions on commands is described below.

---

## Permissions (The Command Keychain Workflow)
Command permissions are handled by a per-command keychain setting in the configuration file. Service modules are in charge of authenticating against this keychain using an `authenticate()` method. The keychain entries appear in this fashion (where service names should match the service folder name):

```yaml
keychain:
  discord: ["everyone"]
  twitch: ["everyone"]
```

The default keychain options provided by auriga.io are listed below:

**Discord**

* `uid:141906786750955521` anyone with the provided user id may run the command
* `rid:141908232498184193` anyone with the provided role id may run the command
* `everyone` anyone and everyone can run the command

**Twitch**

* `broadcaster` broadcaster may run the command
* `moderator` moderators and up may run the command
* `subscriber` paid subscribers and up may run the command
* `una:frosthaven` user frosthaven may also run the command
* `everyone` anyone and everyone man run the command

---

## Running The Platform

Run `npm run auriga` to launch the platform.

---

## More Documentation

* [Built In Command Usage](docs/COMMANDS.md)
* [JSDOCS](http://htmlpreview.github.io/?https://bitbucket.org/fortiscore/fortisbot/raw/master/docs/development/index.html)
* Development Guide (coming soon)

---

I provide this free for all to use and modify, with no small print exceptions.

If you want to support my projects, feel free to [buy me a beer](http://paypal.me/frosthaven)!
